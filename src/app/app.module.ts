import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppPackageModule } from '@applicature-ui/applicature-npm-ui';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppPackageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
